/* eslint-disable require-await */
/** @type {import('next').NextConfig} */
const nextConfig = {
    experimental: {
        outputStandalone: true
    },
    output: 'standalone',
    images: {
        domains: ["cdn.imagin.studio", "storage.stechvn.org"]
    },
    async redirects() {
        return [
            {
                source: '/',
                destination: '/chat',
                permanent: true
            }
        ];
    },
    reactStrictMode: false
};

module.exports = nextConfig;
