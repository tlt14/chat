import { IForwardMessageData, IMessageData, TMessageCallback } from "@/src/types/Message";
import { Socket } from "socket.io-client";



export const sendMessage = (event: string, conversationSocket: Socket<any, any>, cb: TMessageCallback, messageData: IMessageData | IForwardMessageData): void => {
    //temp check
    if(messageData.content === "") {
        alert("Can't send empty message"); 
        return;
    }
    conversationSocket.emit(event, messageData, (result: boolean) => {
        if(!result) {
            console.log("Can't send message");
        } else {
            console.log("send message successfully");
            cb();
        }
    });
};

