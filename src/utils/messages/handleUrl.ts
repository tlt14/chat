export const isUrl = (message: string): boolean => {
    const pattern = /^https:\/\/(?!.*storage\.stechvn\.org)\S+$/;
    console.log(pattern.test(message));
    return pattern.test(message);
};

export const getFileName = (message: string): string => {
    console.log(isUrl(message));
    const parts = message.split('.');
    const fileName = `${parts[parts.length - 2]}.${parts[parts.length - 1]}`;
    console.log(fileName);
    return fileName;
};


const extensions = ["txt", "docx"];
export const checkFile = (message: string): boolean => {
    return extensions.some(e =>
        message.includes(e)
    );
};