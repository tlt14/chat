// import { IConversation } from "@/src/types/Conversation";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const filterConversation = (conversations: any, idUser: string): any => {
    if(!conversations) return [];
    const data = conversations.map((c: any) => {
        
        const otherUserId: any = c.directUserId === idUser ? c.createdBy : c.directUserId;
      
        const conversationName: any = c.createdByUser.id === idUser
            ? c.directUser?.username
            : c.createdByUser?.username;
       
        const avatar = c.createdByUser.id === idUser
            ? c.directUser?.avatar
            : c.createdByUser?.avatar;
        
        const lastLoginOther = c.createdByUser.id === idUser
            ? c.directUser?.lastLogin
            : c.createdByUser?.lastLogin;

        return {
            ...c,
            name: conversationName,
            avatar,
            idOther: otherUserId,
            lastLoginOther
        };
    });
   
    return data;
};

