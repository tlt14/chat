import { IUserSocket } from "@/src/types/User";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export const setLoginCookies = (userData: IUserSocket): void => {
    console.log(userData);
    cookies.set("userLogin", userData);
};

export const getLoginCookies = (): IUserSocket | undefined => {
    const cookiesResult = cookies.get("userLogin");
    if(!cookiesResult) return;
    return cookiesResult;
};

export const deleteLoginCookies = (): void => {
    cookies.remove("userLogin");
};