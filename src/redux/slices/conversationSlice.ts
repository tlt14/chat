
import { IConversation } from "@/src/types/Conversation";
import { IMessage } from "@/src/types/Message";
import { createSlice } from "@reduxjs/toolkit";

interface conversationState {
    otherUsername: string,
    otherAvatar: string,
    lastMessage: IMessage | any,
    listConversation: IConversation[]
}

const initialState: conversationState =  {
    otherUsername: "",
    otherAvatar: "",
    lastMessage: {},
    listConversation: []
};

const conversationSlice = createSlice({
    name: "conversation",
    initialState,
    reducers: {
        setOtherUser: (state, action) => { 
            console.log(action.payload);     
            state = action.payload;
            return state;
        },
        setLastMessage: (state, action) => {
            state.lastMessage = action.payload;
            return state;
        },
        setListConversations: (state, action) => {
            state.listConversation = action.payload;
            return state;
        }
    }
});

export const { setOtherUser, setLastMessage, setListConversations } = conversationSlice.actions;
export default conversationSlice.reducer;