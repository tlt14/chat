import { IUserSocket } from "@/src/types/User";
import { createSlice } from "@reduxjs/toolkit";

const initialState: IUserSocket = {
  active: 1,
  avatar: "",
  background_img: "",
  id: "",
  last_login: 0,
  name: "",
  username: "",
  language: "vn",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserLogin: (state, action) => {
      state = action.payload;
      return state;
    },
    removeUser: (state) => {
      state = initialState;
      return state;
    },
  },
});

export const { setUserLogin, removeUser } = userSlice.actions;
export default userSlice.reducer;
