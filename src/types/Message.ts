import { ISeenUser, IUser } from "./User";


export interface IMessage {
   _id: string,
   conversationId: string,
   type: number,
   content: string,
   seen: ISeenUser[],
   reactions: IUser[],
   isDeleted: boolean,
   createdAt: string,
   updatedAt: string,
   createdBy: ISeenUser
   replyTo?: IMessage,
}

export interface IMessageData {
   content: string,
   replyTo: any,
   conversationId: string,
   type: number
}

export interface IForwardMessageData {
   content: string,
   forward_conversation_id: string,
   type: number,
}

export type TMessageCallback = () => void


