
"use client";
import { ReactElement } from "react";
import "../../globals.css";
import useWindowDimensions from "@/src/hook/useWindowDimension";
import { getLoginCookies } from "@/src/utils/auth/handleCookies";
import { redirect } from "next/navigation";

export default function RootLayout({
    children
}: {
    children: React.ReactNode
  }): ReactElement  {
    const { width } = useWindowDimensions();
    const cookies = getLoginCookies();
    
    //Kiếm tra nếu cookies được trả về
    if(cookies && width > 0) 
        redirect("/chat");
    return <main className="h-full">{children}</main>;
}