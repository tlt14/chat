"use client";
import { setLoginCookies } from "@/src/utils/auth/handleCookies";
import axios from "axios";
import { useRouter } from "next/navigation";
import { ReactElement, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';


type Inputs = {
    username: string,
    password: string,
    checkbox: string,
};

export default function LoginForm(): ReactElement { 
    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const { push } = useRouter();
    const [type, setType] = useState<string>("password");
    

    const handleLoginSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
         
            const result = await axios.post("/api/auth", { ...data, userAgent: navigator.userAgent });
            if(result)  {
                const parsedResult = JSON.parse(result.data);
                setLoginCookies({ ...parsedResult.user });
                alert("Đăng nhập thành công");
                push("/chat");
            }
        } catch(error) {
            console.error(error);
            alert("Đăng nhập không thành công");
        }
    };
   
   
    return (
        <form method="post" onSubmit={handleSubmit(handleLoginSubmit)}  className="flex flex-col gap-y-4 pl-2">
            {/* register your input into the hook by invoking the "register" function */}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full">
                <input key="username" type="text" {...register("username", {
                    required: true
                })} className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"/>
            </div>
            {errors.username && <span className="text-blue-400 font-semibold text-xs">Bạn cần điền thông tin username</span>}
            {/* include validation with required or other standard HTML validation rules */}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full flex items-center gap-x-1">
                <input key="password" type={type} {...register("password", {
                    required: true
                })} className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"/>
                {type === "password" ? <AiFillEye size={20} className="text-thPrimary" onClick={() => setType("text")}/>
                    : <AiFillEyeInvisible size={20} className="text-thPrimary" onClick={() => setType("password")}/>
                } 
            </div>
            {/* errors will return when field validation fails  */}
            {errors.password && <span className="text-yellow-400 font-semibold text-xs">Bạn cần điền thông tin password</span>}
            <div className="flex justify-between items-center px-4">
                <div className="flex items-center gap-x-1">
                    <input key="checkbox" {...register("checkbox")} type="checkbox" value="A" />
                    <span className="text-thWhite opacity-60">Remember me</span>
                </div>
                <p className="text-thPrimary">Forgot password?</p>
            </div>
            <input type="submit" className="cursor-pointer text-thNewtral font-semibold py-2 rounded-full bg-thPrimary" value={"Login"}/>
        </form>
    );
}