import { IconButton, Toolbar, Tooltip } from '@mui/material';
import React, { ReactElement } from 'react';
import { VscListFilter, VscAdd } from "react-icons/vsc";
import { PiTrashSimpleFill } from "react-icons/pi";

interface TableToolbarProps {
    type: number
    numSelected: number
}

const TableToolbar = ({ numSelected, type }: TableToolbarProps): ReactElement => {
    return (
        <Toolbar
            disableGutters={true} // disables gutter padding
        >
            {numSelected > 0 ? (
                <Tooltip title="Delete">
                    <IconButton>
                        <PiTrashSimpleFill size={24} color='white'/>
                    </IconButton>
                </Tooltip>
            ) : (
                <>
                    {type !== 1 && 
                    <Tooltip title="Add">
                        <IconButton>
                            <VscAdd size={24} color='white' />
                        </IconButton>
                    </Tooltip>  
                    }
                    <Tooltip title="Filter list">
                        <IconButton>
                            <VscListFilter size={24} color='white' />
                        </IconButton>
                    </Tooltip>             
                </>
            )}
        </Toolbar>
    );
};

export default TableToolbar;