import React, { ReactElement } from 'react';
import ProfileRadioGroup from '../../profile/[id]/components/ProfileRadioGroup';
import SettingSwitch from './SettingSwitch';
import ProfileInput from '../../profile/[id]/components/ProfileInput';

const SettingNotification = (): ReactElement => {
    return (
        <div className='flex flex-col gap-6 px-6 p-8'>
            <ProfileRadioGroup 
                labelText="Thông báo"
                options={[
                    {
                        label: 'All way on',
                        value: 0
                    },
                    {
                        label: 'Turn off 15 minutes',
                        value: 1
                    },
                    {
                        label: 'Turn off 2 hours',
                        value: 2
                    },
                    {
                        label: 'Turn off 8 hours',
                        value: 3
                    },
                    {
                        label: 'Until i turn it back',
                        value: 4
                    }
                ]}
            />
            <div className='flex justify-between items-center'>
                <span>Hiện trạng thái &quot;Đã xem&quot;</span>
                <SettingSwitch />
            </div>
            <div className='flex justify-between items-center'>
                <span>Ẩn trò chuyện</span>
                <SettingSwitch />
            </div>
            <div>
                <ProfileInput title='Mã PIN' type='text' maxLength={5} />
            </div>
        </div>
    );
};

export default SettingNotification;