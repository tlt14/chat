import React, { ReactElement } from 'react';
import LayoutBox from '../components/LayoutBox';
import SettingNotification from './components/SettingNotification';
import TableView from './components/TableView';
import { GridColDef } from '@mui/x-data-grid';

// Sample Data
const columnsLoginHistory: GridColDef[] = [
    { field: 'time', headerName: 'Time', width: 250, sortable: false },
    { field: 'device', headerName: 'Device', width: 250, sortable: false },
    { field: 'method', headerName: 'Login Methods', width: 250, sortable: false },
    { field: 'location', headerName: 'Location', width: 250, sortable: false }
];

const rowsLoginHistory = [
    { id: 1, time: '15 May 2020 9:30 am', device: 'Iphone 13', method: 'Login Methods', location: 'Location' },
    { id: 2, time: '15 May 2020 9:30 am', device: 'Window', method: 'Login Methods', location: 'Location' },
    { id: 3, time: '15 May 2020 9:30 am', device: 'Samsung Galaxy S22', method: 'Login Methods', location: 'Location' },
    { id: 4, time: '15 May 2020 9:30 am', device: 'Oppo', method: 'Login Methods', location: 'Location' },
    { id: 5, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' },
    { id: 6, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' },
    { id: 7, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' },
    { id: 8, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' }
];

const columnsWalletAddress: GridColDef[] = [
    { field: 'time', headerName: 'Time', width: 250, sortable: false },
    { field: 'address', headerName: 'Address', width: 350, sortable: false }
];

const rowsWalletAddress = [
    { id: 1, time: '15 May 2020 9:30 am', address: '19GfBxN2QxFgD8UHQWme2p5My4Sagm7Nip' },
    { id: 2, time: '15 May 2020 9:30 am', address: '19GfBxN2QxFgD8UHQWme2p5My4Sagm7Nip' },
    { id: 3, time: '15 May 2020 9:30 am', address: '19GfBxN2QxFgD8UHQWme2p5My4Sagm7Nip' }
];

const Setting = (): ReactElement => {
    return (
        <div className='flex flex-col gap-8 p-8 text-base lg:text-lg'>
            <h1 className='font-bold text-2xl'>Setting</h1>
            {/* Login history*/}
            <LayoutBox>
                <div className='flex flex-1 flex-col px-6 pt-4 pb-2'>
                    <TableView 
                        title='Lịch sử đăng nhập' 
                        type={1} //login history
                        columns={columnsLoginHistory} 
                        rows={rowsLoginHistory}
                    />
                </div>
            </LayoutBox>
            {/* Wallet and notifi */}
            <div className='grid grid-cols-1 xl:grid-cols-3 gap-8'>
                <div className='col-span-1 xl:col-span-2 flex flex-1 flex-col gap-8'>
                    <LayoutBox>
                        <div className='flex flex-1 flex-col px-6 pt-4 pb-2'>
                            <TableView 
                                title='Lịch sử ví'
                                type={2} //wallet history
                                columns={columnsWalletAddress}
                                rows={rowsWalletAddress}
                            />
                        </div>
                    </LayoutBox>
                </div>
                <div className='col-span-1'>
                    <LayoutBox>
                        <SettingNotification />
                    </LayoutBox>
                </div>
            </div>
        </div>
    );
};

export default Setting;