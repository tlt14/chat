"use client";
import { ReactElement, useState, use, cache, useEffect } from "react";
import Image from "next/image";
import { IoIosChatbubbles } from "react-icons/io";
// import { AiFillCheckCircle } from "react-icons/ai";
import GroupIcon from "@/public/profile-2user.svg";
import RequestIcon from "@/public/sms-notification.svg";
import axios from "axios";
import { IConversation } from "@/src/types/Conversation";

// import Pattern from "@/public/pattern.png";
// import Pattern2 from "@/public/pattern2.jpg";
// import Pattern3 from "@/public/pattern3.jpg";
import Link from "next/link";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setListConversations, setOtherUser } from "@/src/redux/slices/conversationSlice";
import moment from "moment";
import { MESSAGE_TYPE } from "@/src/constants/chat";
import { AiFillCheckCircle } from "react-icons/ai";



const fetchConversations = cache(() => axios.get("/api/conversations"));
export default function LeftBar(): ReactElement{
    const [option, setOption] = useState<string>("chat");
    const dispatch = useAppDispatch();
    const { user, conversation }  = useAppSelector((state) => state.user);
    const { data } = use(fetchConversations());
    const [listConversation, setListConversation] = useState<IConversation[]>(filterConversation(data, user.id));
    dispatch(setListConversations(listConversation));
    const currentDate = moment();
    const unreadMessages = listConversation.reduce((total: number, conversation: IConversation) => total + conversation.unSeenMessageTotal, 0);
   

    useEffect(() => {
        console.log("test");
        // if(Object.keys(conversation.lastMessage) || !conversation.lastMessage) return;
        (async () => {
            const { data } = await axios.get("/api/conversations");
            console.log(data);
            setListConversation(filterConversation(data, user.id));
        })();
    }, [conversation.lastMessage]);
   
   

    return (
        <div className="relative lg:flex-grow-0 lg:flex-shrink-0 md:flex-shrink-0">
            {
                option === "chat" ? (<div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl  h-[90vh] lg:h-[85vh] mx-4 mt-4 overflow-y-auto">
                    <div className="flex gap-x-1 items-center">
                        <h1 className="text-xl font-semibold">Message</h1>
                        <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">{unreadMessages}</p>
                    </div>
                    <div className="flex flex-col gap-y-2">
                        {
                            listConversation.map((c: IConversation | any) => {
                                
                                const checkNewMsg = conversation.lastMessage?.conversationId === c._id;
                                const userNewMessage = conversation.lastMessage?.createdBy?._id === user.id ? "Bạn: " : `${conversation.lastMessage?.createdBy?.username}: `;
                                const userMessage = c.latestMessage?.createdBy?._id === user.id ? "Bạn: " : `${c.latestMessage?.createdBy?.username}: `;
                                const targetDate = moment(c?.latestMessage?.createdAt);
                                const checkDate = targetDate.isBefore(currentDate, 'day');
                                return (
                                    <Link href={`/chat/conversation/${c._id}`} key={c._id}>
                                        <div onClick={() => dispatch(setOtherUser({
                                            otherUsername: c?.name,
                                            otherAvatar: c?.avatar,
                                            lastMessage: conversation.lastMessage || {}
                                        }))}  className="flex gap-x-2 items-center py-2 px-2 hover:bg-thNewtral1 rounded-lg">
                                       
                                            <Image 
                                                src={c?.avatar}
                                                sizes="(max-width: 56px) 56px, 56px"
                                                alt={"Logo"}
                                                loading="eager"
                                                priority
                                                width={56}
                                                height={56}
                                                className="rounded-full object-cover h-[56px]"
                                            />
                                            <div className="flex gap-x-8 justify-between h-full items-center flex-grow">
                                                <div>
                                                    <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">{c?.name}</p>
                                                    <span className={`${checkNewMsg && "font-bold"} text-[.8125rem] opacity-80 max-w-[195px] line-clamp-1`}>
                                                        {checkNewMsg ? 
                                                            (conversation.lastMessage.type === MESSAGE_TYPE.IMAGE || conversation.lastMessage.type === MESSAGE_TYPE.STICKER ? <Image alt="test" src={conversation.lastMessage.content} width={30} height={30} /> : <span>{`${userNewMessage}${conversation.lastMessage.content}`}</span>) 
                                                            :      
                                                            (c?.latestMessage?.type === MESSAGE_TYPE.IMAGE || c?.latestMessage?.type === MESSAGE_TYPE.STICKER ? <Image alt="test" src={c?.latestMessage?.content} width={30} height={30} /> : <span>{`${userMessage}: ${c?.latestMessage?.content}`}</span>) 
                                                        }
                                                    </span>
                                                </div>
                                                <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                    
                                                    <span className="text-xs opacity-60">{ !checkDate ? moment(c?.latestMessage?.createdAt).format('hh:mm') :  moment(c?.latestMessage?.createdAt).format('DD/MM')}</span>
                                                    {c?.latestMessage?.seen.length > 1 ? (
                                                        <>
                                                            <Image 
                                                                src={user.avatar}
                                                                sizes="(max-width: 16px) 16px, 16px"
                                                                alt={"Logo"}
                                                                loading="eager"
                                                                priority
                                                                width={16}
                                                                height={16}
                                                                className="rounded-full object-cover h-4"
                                                            />
                                                        </>
                                                    ) : (
                                                        <>
                                                            <AiFillCheckCircle size={16} className="text-thPrimary"/>
                                                        </>
                                                    )}    
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                );})
                        }
                    </div>
                </div>) : option === "request" ?  (<div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl  h-[90vh] lg:h-[85vh] mx-4 mt-4 overflow-y-auto">
                    <div className="flex gap-x-1 items-center">
                        <h1 className="text-xl font-semibold">Request</h1>
                        <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">50</p>
                    </div>
                    <div className="flex flex-col gap-y-2">
                    </div>
                </div>) : null
            }
            
            <div className="absolute -bottom-4 left-1/2 -translate-x-1/2 flex items-center px-4 py-2 rounded-2xl gap-x-8 bg-thNewtral1 md:w-[200px]">
                <div className={`${option === 'chat' ? 'text-thPrimary' : 'text-thNewtral2'}  cursor-pointer`} onClick={() => setOption("chat")}>
                    <IoIosChatbubbles size={40} />
                </div>
                <div className={`${option === 'group' ? 'text-thPrimary' : 'text-thNewtral2'}  cursor-pointer`} >
                    <Image 
                        src={GroupIcon}
                        sizes="(max-width: 40px) 40px, 40px"
                        alt={"Logo"}
                        loading="eager"
                        priority
                        width={40}
                        height={40}
                        className="rounded-full object-cover"
                    />
                </div>
                <div className={`${option === 'request' ? 'text-thPrimary' : 'text-thNewtral2'}  cursor-pointer`} onClick={() => setOption("request")}>
                    <Image 
                        src={RequestIcon}
                        sizes="(max-width: 40px) 40px, 40px"
                        alt={"Logo"}
                        loading="eager"
                        priority
                        width={40}
                        height={40}
                        className="rounded-full object-cover"
                    />
                </div>
            </div>
        </div>
    );
};

