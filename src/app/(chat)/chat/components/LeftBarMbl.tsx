"use client";
import { ReactElement, useState, use, cache, useEffect } from "react";
import Image from "next/image";
import { IoIosChatbubbles } from "react-icons/io";
import GroupIcon from "@/public/profile-2user.svg";
import RequestIcon from "@/public/sms-notification.svg";
import { IConversation } from "@/src/types/Conversation";
import Link from "next/link";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setOtherUser } from "@/src/redux/slices/conversationSlice";
import axios from "axios";
import moment from "moment";
import { Avatar } from "./Avatar";
import { MESSAGE_TYPE } from "@/src/constants/chat";
import { AiFillCheckCircle } from "react-icons/ai";
// interface User {
//     id: number;
//     avatar: string;
//     name: string;
//     time: string;
//     message: string;
//     messageType: number;
//     status: string;
//   }

const fetchConversations = cache(() => axios.get("/api/conversations"));

export default function LeftBarMbl(): ReactElement{
    // const [idConversation,setIdconversation] = useState<number>(0);
    const [Option, setOption] = useState<string>("chat");
    const dispatch = useAppDispatch();
    const { user, conversation }  = useAppSelector((state) => state.user);
    const { data } = use(fetchConversations());
    const [listConversation, setListConversation] = useState(filterConversation(data, user.id));
    const currentDate = moment();
    useEffect(() => {
        console.log("test");
        // if(Object.keys(conversation.lastMessage) || !conversation.lastMessage) return;
        (async () => {
            const { data } = await axios.get("/api/conversations");
            console.log(data);
            setListConversation(filterConversation(data, user.id));
        })();
    }, [conversation.lastMessage]);
    return (
        <div className="relative flex-grow-0 flex-shrink-0">
            {
                Option === "chat" ? (
                    <div className="flex flex-col gap-y-4 flex-shrink-0 h-[85vh] mx-4 mt-4 overflow-y-auto">
                        <div className="flex gap-x-1 items-center">
                            <h1 className="text-xl font-semibold">Message</h1>
                            <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">20</p>   
                        </div>
                        <div className="flex flex-col gap-y-2">
                            {
                                listConversation.map((item: IConversation | any) => {
                                
                                    const checkNewMsg = conversation.lastMessage?.conversationId === item._id;
                                    const userNewMessage = conversation.lastMessage?.createdBy?._id === user.id ? "Bạn: " : conversation.lastMessage?.createdBy?.username;
                                    const userMessage = item.latestMessage?.createdBy?._id === user.id ? "Bạn: " : item.latestMessage?.createdBy?.username;
                                    const targetDate = moment(item?.latestMessage?.createdAt);
                                    const checkDate = targetDate.isBefore(currentDate, 'day');
                                    return (
                                        <Link href={`/chat/conversation/${item._id}`} key={item._id}>
                                            <div className="flex gap-x-2 items-center py-2 hover:bg-thNewtral1 rounded-lg" onClick={() => 
                                                dispatch(setOtherUser({ 
                                                    otherUsername: item?.name, 
                                                    otherAvatar: item?.avatar 
                                                }))}>
                                                <div className="relative rounded-full h-14 w-14 overflow-hidden">
                                                    <Avatar url={item?.avatar} />
                                                </div>
                                                
                                                <div className="flex gap-x-16 justify-between h-full items-center w-full">
                                                    <div>
                                                        <p className="text-[.9375rem] font-semibold max-w-[185px] text-ellipsis line-clamp-1">{item?.name}</p>
                                                        <span className={`text-[.8125rem] opacity-80 max-w-[195px] line-clamp-1 ${checkNewMsg && "font-bold"}`}>
                                                            {checkNewMsg ? 
                                                                (conversation.lastMessage.type === MESSAGE_TYPE.IMAGE || conversation.lastMessage.type === MESSAGE_TYPE.STICKER ? <Image alt="test" src={conversation.lastMessage.content} width={30} height={30} /> : <span>{`${userNewMessage}${conversation.lastMessage.content}`}</span>) 
                                                                :      
                                                                (item?.latestMessage?.type === MESSAGE_TYPE.IMAGE || item?.latestMessage?.type === MESSAGE_TYPE.STICKER ? <Image alt="test" src={item?.latestMessage?.content} width={30} height={30} /> : <span>{`${userMessage}: ${item?.latestMessage?.content}`}</span>) 
                                                            }
                                                        </span>
                                                    </div>
                                                    <div className="flex flex-col justify-between gap-y-1 items-center">
                                                        <span className="text-xs opacity-60">                                        
                                                            { !checkDate ? moment(item?.latestMessage?.createdAt).format('hh:mm') :  moment(item?.latestMessage?.createdAt).format('DD/MM')}
                                                        </span>
                                                        {item?.latestMessage?.seen.length > 1 ? (
                                                            <>
                                                                <Image 
                                                                    src={user.avatar}
                                                                    sizes="(max-width: 16px) 16px, 16px"
                                                                    alt={"Logo"}
                                                                    loading="eager"
                                                                    priority
                                                                    width={16}
                                                                    height={16}
                                                                    className="rounded-full object-cover h-4"
                                                                />
                                                            </>
                                                        ) : (
                                                            <>
                                                                <AiFillCheckCircle size={16} className="text-thPrimary"/>
                                                            </>
                                                        )}  
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    );
                                })
                            }
                        </div>
                    </div>) : Option === "request" ?  (
                    <div className="flex flex-col gap-y-6 flex-shrink-0 h-[85vh] mx-4 mt-4 overflow-y-auto">
                        <div className="flex gap-x-1 items-center">
                            <h1 className="text-xl font-semibold">Request</h1>
                            <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">20</p>
                        </div>
                        <div className="flex flex-col gap-y-2">
                            {
                                listConversation.map((item: IConversation | any)=>(
                                    <Link href={`/chat/conversation/${item._id}`} key={item._id}>
                                        <div className="flex gap-x-2 items-center py-2 hover:bg-thNewtral1 rounded-lg" onClick={() => 
                                            dispatch(setOtherUser({ otherUsername: item?.name, otherAvatar: item?.avatar 
                                            }))}>
                                            <Image 
                                                src={item?.avatar}
                                                sizes="(max-width: 56px) 56px, 56px"
                                                alt={"Logo"}
                                                loading="eager"
                                                priority
                                                width={56}
                                                height={56}
                                                className="rounded-full object-cover h-[56px]"
                                            />
                                            <div className="flex gap-x-16 justify-between h-full items-center w-full">
                                                <div>
                                                    <p className="text-[.9375rem] font-semibold">{item?.name}</p>
                                                    <span className="text-[.8125rem] opacity-80 max-w-[195px] line-clamp-1">
                                                        {item?.latestMessage?._id === user.id ? "you: " : ""} 
                                                        {item?.latestMessage?.content}</span>
                                                </div>
                                                <span className="text-xs opacity-60">                                        
                                                    {moment(item?.updatedAt).format("HH:mm")}
                                                </span>
                                            </div>
                                        </div>
                                    </Link>
                                ))
                            }
                        </div>
                    </div>
                ) : null
            }
            
            <div className="absolute -bottom-8 left-1/2 -translate-x-1/2 flex items-center px-4 py-2 rounded-2xl gap-x-8 bg-thNewtral1">
                <div className="text-thPrimary cursor-pointer" onClick={()=>setOption("chat")}>
                    <IoIosChatbubbles size={40} />
                </div>
                <div className="text-thNewtral2 hover:text-thNewtral cursor-pointer" >
                    <Image 
                        src={GroupIcon}
                        sizes="(max-width: 40px) 40px, 40px"
                        alt={"Logo"}
                        loading="eager"
                        priority
                        width={40}
                        height={40}
                        className="rounded-full object-cover"
                    />
                </div>
                <div className="text-thNewtral2 hover:text-thNewtral cursor-pointer" onClick={()=>setOption("request")}>
                    <Image 
                        src={RequestIcon}
                        sizes="(max-width: 40px) 40px, 40px"
                        alt={"Logo"}
                        loading="eager"
                        priority
                        width={40}
                        height={40}
                        className="rounded-full object-cover"
                    />
                </div>
            </div>
        </div>
    );
};

