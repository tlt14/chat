import React, { ReactElement } from 'react';

interface Props {
    children: ReactElement;
}

const LayoutBox = ({ children }: Props): ReactElement => {

    return (
        <div className='flex w-full flex-col justify-center bg-thNewtral1 rounded-2xl overflow-hidden'>
            {children}
        </div>
    );
};

export default LayoutBox;