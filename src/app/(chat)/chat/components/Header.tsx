"use client";
import Image from "next/image";
import { ReactElement, useEffect, useState  } from "react";
import styles from "./layout.module.css";
import Logo from "@/public/DP 1.png";
import { BiSearch } from "react-icons/bi";
import { IoMdSettings } from "react-icons/io";
import MenuSetting from "./MenuSetting/MenuSetting";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { getLoginCookies } from "@/src/utils/auth/handleCookies";
import { setUserLogin } from "@/src/redux/slices/userSlice";
import axios from "axios";
import io from "socket.io-client";
import { Avatar } from "./Avatar";
import { useRouter } from "next/navigation";



export default function Header(): ReactElement {
    const [isSetting, setIsSetting] = useState<boolean>(false);
    const { user }  = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const cookieData = getLoginCookies();
    const { push } = useRouter();

    useEffect(() => {
        (async () => {
            console.log("test user");
            if(cookieData) {
                const { data } = await axios.get(`/api/auth/chat-token`);
                if(!data) return;
                const socket = io(`${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}`, {
                    auth: { token: data },
                    autoConnect: true,
                    transports: ["websocket"],
                    withCredentials: true
                });
                console.log(socket);
                dispatch(setUserLogin({ ...cookieData, socket }));               
            }
        })();
    }, []);
    
    return (

        <div className="flex justify-between items-center my-3 mx-6 relative">
            <Image src={Logo}
                alt={"Logo"}
                loading="eager"
                priority
                width={43.702}
                height={43.702}
                className="bg-thNewtral1 rounded-full object-cover hidden md:block lg:block"
            />
            <div className="feature_area flex gap-x-2 items-center justify-between lg:justify-normal md:justify-normal w-full md:w-fit lg:w-fit">
                <div className={`h-[40px] flex items-center gap-2 bg-thNewtral1 p-2 w-[70%] md:w-[300px] lg:w-[300px] rounded-lg ${styles.search}`}>
                    <div className="opacity-50"><BiSearch /></div>
                    <input type="text" className="bg-transparent border-none outline-none text-sm font-normal" placeholder="Search"/>
                </div>
                <div className="flex gap-x-2 items-center">
                    <div className="grid place-items-center p-2 rounded bg-thNewtral1 h-[40px] w-[40px] hover:bg-thNewtral cursor-pointer" onClick={() => setIsSetting(!isSetting)}>
                        <IoMdSettings size={24}/>
                    </div>
                    <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden"
                        onClick={() => push(`/chat/profile/${user.id}`)}
                    >
                        <Avatar url={user.avatar} />
                    </div>
                </div>
            </div>
            {
                isSetting && <MenuSetting socket={user.socket} />
            }
        
        </div>
    );
}
