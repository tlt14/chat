import React, { ReactElement } from 'react';
import Image from 'next/image';
interface Props {
    url: string
}
export const Avatar = ({ url }: Props): ReactElement => {
    console.log("src", url);
    return (
        <Image 
            sizes="500px"
            src={url}
            alt='avatar'
            loading="eager"
            fill
            className='object-cover absolute'
        />
    );
};
