import React, { ReactElement } from 'react';
import Image from 'next/image';
import { TiContacts } from "react-icons/ti";
import { IoMdSettings } from "react-icons/io";
import { IoLogOutOutline } from "react-icons/io5";
import LanguageIcon from '@/public/language.svg';
import MenuItem from './MenuItem';
import { useRouter } from 'next/navigation';
import { deleteLoginCookies } from '@/src/utils/auth/handleCookies';
// import axios from "axios";
// import { useAppDispatch } from '@/src/redux/hook';
// import { removeUser } from '@/src/redux/slices/userSlice';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const MenuSetting = ({ socket }: any): ReactElement => {
    // const dispatch = useAppDispatch();
    const router = useRouter();
    const menu = [
        {
            title: 'Giao diện',
            subTitle: 'Tối',
            children: <IoMdSettings size={24} />,
            onClick: () => {}
        },
        {
            title: 'Ngôn ngữ',
            children: <Image src={LanguageIcon} width={24} alt='language' />,
            onClick: () => {}
        },
        {
            title: 'Danh bạ',
            children: <TiContacts size={24} />,
            onClick: () => router.push('/chat/contact')
        },
        {
            title: 'Thiết lập',
            children: <IoMdSettings size={24} />,
            onClick: () => router.push('/chat/setting')
        },
        {
            title: 'Đăng xuất',
            children: <IoLogOutOutline size={24} />,
            onClick: () => handleLogout()
        }
    ];

    // eslint-disable-next-line require-await
    const handleLogout = async () => {
        deleteLoginCookies();
        if(socket) socket.disconnect();
        // dispatch(removeUser());
        router.push("/login");
        // try {
        //     const result = await axios.post("/api/logout");
        //     if (result) {
        //         deleteLoginCookies();
        //         if(socket) socket.disconnect();
        //         // dispatch(removeUser());
        //         router.push("/login");
        //     }
        // } catch (err) {
        //     console.log(err);
        // }
    };

    return (
        <div className="
            absolute 
            right-12 
            top-12 
            bg-thNewtral1
            rounded-lg 
            overflow-hidden
            shadow-2xl
            transition-all
            duration-200
            ease-in-out
            w-72
            p-3
            z-50
            animate-c-show-popup
            origin-top-right
            ">
            {
                menu.map(item => 
                    <MenuItem 
                        key={`menu-${item.title}`} 
                        title={item.title} 
                        subtitle={item.subTitle} 
                        onClick={item.onClick}
                    >
                        {item.children}
                    </MenuItem>
                )
            }
        </div>
    );
};

export default MenuSetting;