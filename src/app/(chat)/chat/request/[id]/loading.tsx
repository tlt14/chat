import styles from "./request.module.css";
export default function Loading(): JSX.Element {
    // You can add any UI inside Loading, including a Skeleton.
    return (
        <div className="w-full h-full px-4 flex flex-col justify-between py-5">
            <div className="flex flex-col justify-center items-center gap-y-2">
                <div className={`w-20 h-20 rounded-full ${styles.gradient}`}></div>
                <div className={`w-32 h-2 rounded ${styles.gradient}`}></div>
                <div className={`w-20 h-1.5 rounded ${styles.gradient}`}></div>
            </div>
            <div className="flex flex-col gap-y-2">
                <div className="flex gap-x-1 items-end">
                    <div className={`w-4 h-4 rounded-full`}></div>
                    <div className={`w-56 h-12 ${styles.gradient} ${styles.partner_box}`}></div>
                </div>
                <div className="flex gap-x-1 items-end">
                    <div className={`w-4 h-4 rounded-full`}></div>
                    <div className={`w-32 h-12 ${styles.gradient} ${styles.partner_box}`}></div>
                </div>
                <div className="flex gap-x-1 items-end">
                    <div className={`w-4 h-4 rounded-full ${styles.gradient}`}></div>
                    <div className={`w-72 h-24 ${styles.gradient} ${styles.partner_box}`}></div>
                </div>
                <div className="flex justify-end w-full">
                    <div className={`w-56 h-16 ${styles.gradient} ${styles.owner_box}`}></div>
                </div>
            </div>
        </div> 
    );
}