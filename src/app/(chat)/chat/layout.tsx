
"use client";
import { ReactElement } from "react";
import "../../globals.css";
import Header from "./components/Header";
import LeftBar from "./components/LeftBar";
import useWindowDimensions from "../../../hook/useWindowDimension";
import React from "react";

import { getLoginCookies } from "@/src/utils/auth/handleCookies";
import { redirect } from "next/navigation";

export default function RootLayout({
    children
}: {
  children: React.ReactNode;
}): ReactElement {
    const { width } = useWindowDimensions();
    const cookies = getLoginCookies();
    console.log(cookies);
    //Kiếm tra nếu cookies được trả về
    if(!cookies && width > 0) 
        redirect("/login");
    return (
        <main className='relative'>
            <Header />
            {
                width > 480 ? (<div className="flex items-center">
                    <LeftBar />
                    <div className="bg-thNewtral mt-4 mr-4 w-full h-[90vh] lg:h-[85vh] rounded-2xl relative overflow-hidden overflow-y-auto">
                        {children}
                    </div>
                </div>) : (<div className="h-[92vh] md:h-full lg:h-full">
                    {children}
                </div>)
            }
                
        </main>
    );
}
