"use client";
import React, { ReactElement, cache, use, useState, useEffect } from 'react';
import LayoutBox from '../../../../(chat)/chat/components/LayoutBox';
import ProfileCard from './components/ProfileCard';
import PersonalInfo from './components/PersonalInfo';
import PersonalBio from './components/PersonalBio';
import axios from 'axios';
import { IUserProfile } from '@/src/types/User';

const fetchProfile = cache((id: string) => axios.get(`/api/profile?id=${id}`));
const updateProfile = cache((body: IUserProfile) => axios.put(`/api/profile/`, body));

const Profile = ({ params }: { params: { id: string } }): ReactElement => {
    const { data } = use(fetchProfile(params.id));
    const [userInfo, setUserInfo] = useState(data.data); // giá trị cố định
    const [infoValue, setInfoValue] = useState(data.data);
    
    useEffect(() => {
        setInfoValue(userInfo);
    }, [userInfo]);

    const handleSaveProfile = async () => {
        const postValue = infoValue;
        try {
            const { data } = await updateProfile(postValue);
            if (data) {
                setUserInfo(data.data);
                alert("Lưu thành công");
            }
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className='grid grid-cols-1 lg:grid-cols-2 text-white rounded-2xl p-4 gap-8 text-sm lg:text-base'>
            <div className='flex flex-1 flex-col gap-8'>
                <LayoutBox>
                    <ProfileCard userInfo={userInfo} setUserInfo={setUserInfo} handleSave={handleSaveProfile} />
                </LayoutBox>

                <LayoutBox>
                    <PersonalInfo userInfo={infoValue} setUserInfo={setInfoValue} />
                </LayoutBox>
            </div>
            <div className='flex flex-1 flex-col gap-8'>
                <LayoutBox>
                    <PersonalBio userInfo={infoValue} setUserInfo={setInfoValue} />
                </LayoutBox>
            </div>
        </div>
    );
};

export default Profile;
