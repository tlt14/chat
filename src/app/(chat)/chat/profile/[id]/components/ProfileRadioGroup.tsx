"use client";
import { useState, ReactElement } from "react";
import ProfileRadioOption from "./ProfileRadioOption";

interface RadioGroupProps {
    options: { label: string, value: any }[];
    onChange?: (selectedIndex: number) => void;
    value?: number;
    labelText?: string;
}

const ProfileRadioGroup = ({ options, onChange, value = 1, labelText }: RadioGroupProps): ReactElement => {
    const [selectedIndex, setSelectedIndex] = useState<number>(value);

    const onSelect = (index: number) => {
        setSelectedIndex(index);
        if(onChange) {
            onChange(index);
        }
    };

    return (
        <div className="flex flex-col gap-2 w-fit">
            <span>{labelText}</span>
            {options.map((option) => (
                <ProfileRadioOption
                    key={option.value}
                    index={option.value}
                    selectedIndex={selectedIndex}
                    onSelect={(index) => onSelect(index)}
                >
                    {option.label}
                </ProfileRadioOption>
            ))}
            
        </div>
    );
};

export default ProfileRadioGroup;