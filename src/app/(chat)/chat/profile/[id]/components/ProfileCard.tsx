"use client";
import React, { ReactElement, useState, cache, use } from 'react';
import Image from 'next/image';
import EditIcon from '@/public/edit.svg';
import LoadingIcon from '@/public/loader.svg';
import ProfileButton from './ProfileButton';
import { IUserProfile } from '@/src/types/User';
import axios from 'axios';

interface ProfileCardProps {
    userInfo: IUserProfile;
    setUserInfo: (userInfo: IUserProfile) => void; 
    handleSave: () => void;
}

const getUserFollower = cache((id: string) => axios.get(`/api/follower?id=${id}`));
const getUserFollowing = cache((id: string) => axios.get(`/api/following?id=${id}`));
const updateBackground = cache((file: any) => axios.post(`/api/upload`, file, {
    headers: { "Content-Type": "multipart/form-data" }
}));
const updateProfile = cache((body: IUserProfile) => axios.put(`/api/profile/`, body));

const ProfileCard = ({ userInfo, setUserInfo, handleSave }: ProfileCardProps): ReactElement => {

    const [loadingBackground, setLoadingBackground] = useState(false);

    const follower_res = use(getUserFollower(userInfo.id));
    const following_res = use(getUserFollowing(userInfo.id));

    const followers = follower_res.data.data;
    const followings = following_res.data.data;

    const handleDeleteAccount = (): void => {
        console.log("delete account");
    };

    const handleBackgroundChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
        if(!event.target.files) return;
        const file: File = event.target.files[0];

        const fileData = new FormData();
        fileData.append('files', file);
        
        if (file) {
            try {
                setLoadingBackground(true);
                const res: any = await updateBackground(fileData); 
                if (res.data.success) {
                    try {
                        const updatedValue = { ...userInfo, background_img: res.data.data[0].link };
                        const res2: any = await updateProfile(updatedValue);
                        if (res2.data.success) {
                            setUserInfo(res2.data.data);
                            alert("Đổi hình nền thành công");
                        }
                        else {
                            alert("Đổi hình nền không thành công");
                        }
                    } catch (err) {
                        console.log(err);
                    }
                }
            } catch (error) {
                console.log('Error uploading background image:', error);
            } finally {
                setLoadingBackground(false);
            }
        }
    };

    return (
        <div className='flex flex-col'>
            {/* Background container*/}
            <div className='relative h-32'>
                <Image 
                    src={userInfo.background_img}
                    alt='wallpaper'
                    fill
                    style={{ objectFit: 'cover' }}
                />
                <label className='absolute top-3 right-3 rounded-full p-2 bg-[#22242699] cursor-pointer' 
                    htmlFor='backgroundInput'
                >
                    <Image
                        src={loadingBackground ? LoadingIcon : EditIcon}
                        alt='edit background image'
                        width={24}
                    />
                    <input
                        id='backgroundInput'
                        type='file'
                        accept='image/*'
                        style={{ display: 'none' }}
                        onChange={handleBackgroundChange}
                    />
                </label>
            </div>
            {/* Avatar and information container */}
            <div className='px-4 pb-7 relative'>
                {/* Avatar and follow */}
                <div className='
                flex 
                justify-center
                mt-20
                mb-5
                xl:mt-8
                xl:justify-end     
            '>
                    <div className='
                    absolute 
                    top-0
                    xl:left-4 
                    border-8 
                    border-thNewtral1
                    w-36 
                    h-36 
                    overflow-hidden 
                    rounded-full 
                    -translate-y-1/2
                '>
                        <Image 
                            src={userInfo.avatar}
                            alt='avatar'
                            fill
                            style={{ objectFit: 'cover' }}
                        />
                    </div>
                    <div className='flex flex-col gap-2 xl:flex-row'>
                        <div className='flex gap-2'>
                            <span className='font-semibold'>{followers.length}</span>
                            <span>Người theo dõi</span>
                        </div>
                        <div className='flex gap-2'>
                            <span className='font-semibold'>{followings.length}</span>
                            <span>Đang theo dõi</span>
                        </div>
                    </div>     
                </div>
                {/* User info and button */}
                <div>
                    <div className='mb-4'>
                        <h1 className='font-semibold'>
                            {userInfo.name}
                        </h1>
                        <span>{userInfo.bio}</span>
                    </div>
                    <div className='flex flex-col justify-start items-start gap-4 md:flex-row md:items-center'>
                        <ProfileButton type='danger' onClick={() => handleDeleteAccount()} >
                            <span>
                                Xóa tài khoản
                            </span>
                        </ProfileButton >
                        <ProfileButton type='primary' onClick={handleSave} >
                            <span>
                                Lưu
                            </span>
                        </ProfileButton >
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProfileCard;