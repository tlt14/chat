import React, { ChangeEvent, ReactElement } from 'react';
interface InputProps {
    title: string;
    type: string;
    maxLength?: number;
    value?: any;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}

const ProfileInput = ({ title, type, maxLength, value, onChange }: InputProps): ReactElement => {
    return (
        <div className='flex flex-col gap-2'>
            <span>{title}</span>
            <input 
                value={value}
                onChange={onChange}
                type={type}
                maxLength={maxLength}
                className={`${type === 'date' && 'calendar-white'} appearance-none outline-none bg-thNewtral p-3 rounded-lg`}
            />
        </div>
    );
};

export default ProfileInput;