"use client";
import React, { ReactElement, useState } from 'react';
import Image from "next/image";
// import Pattern from "@/public/pattern.png";
// import Pattern2 from "@/public/pattern2.jpg";
// import Pattern3 from "@/public/pattern3.jpg";
import { AiFillCheckCircle } from "react-icons/ai";
import { useAppSelector } from '@/src/redux/hook';
import { IConversation } from '@/src/types/Conversation';
import { Socket } from 'socket.io-client';
import { IForwardMessageData, IMessage } from '@/src/types/Message';
import { sendMessage } from '@/src/utils/messages/sendMessage';
import { EVENT_NAMES } from '@/src/constants/chat';


interface Content {
    message: IMessage,
    socket: Socket<any, any>
}
export default function Forward( { message, socket }: Content ): ReactElement { 
    const [isChecked, setIsChecked] = useState<string>("");
    const { conversation } = useAppSelector(state => state.user);
    console.log(conversation);
    const handleForward = () => {
        const forwardMessage: IForwardMessageData = {
            content: message.content,
            type: message.type,
            forward_conversation_id: isChecked
        };
        sendMessage(EVENT_NAMES.CONVERSATION.FORWARD_MESSAGE, socket, () => {
        }, forwardMessage);
    };
   
    return (
        <>
            
            <div className='bg-thNewtral1 rounded-xl px-4'>
                <p className='py-1.5 text-center text-lg'>Chia sẽ</p> 
                <div className='w-full bg-thNewtral rounded'>
                    <input type="text" placeholder='tìm kiếm bạn bè' className='px-2 py-2 bg-transparent outline-none '/>
                </div>
                <div className='list_user py-4 max-h-[600px] overflow-y-auto'>
                    {
                        conversation.listConversation.map((conversation: IConversation | any)=>(
                            <div key={conversation._id} className='py-2 flex items-center gap-x-10 justify-between'>
                                <div className='flex gap-x-3 items-center'>
                                    <Image 
                                        src={conversation?.avatar}
                                        sizes="(max-width: 44px) 44px, 44px"
                                        alt={"avt"}
                                        loading="eager"
                                        priority
                                        width={44}
                                        height={44}
                                        className="rounded-full h-11 cursor-pointer"
                                    />
                                    <div className='flex flex-col '>
                                        <p className='text-sm font-normal'>{conversation?.name}</p>
                                        <span className='text-xs opacity-60'>{conversation?.name}</span>
                                    </div>
                                </div>
                                {
                                    isChecked === conversation?.id ? (<AiFillCheckCircle size={24} onClick={() => setIsChecked("")} className='cursor-pointer'/>) : (<div className='h-6 w-6 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer' onClick={() => setIsChecked(conversation?.id)}></div>)
                                }
                            </div>
                        ))
                    }
                </div>
                <div className='py-3'>
                    <button className='bg-thPrimary text-thDark-background w-full py-1 rounded-md' onClick={handleForward}>Gửi</button>
                </div>
            </div>
        </>
    );
}