import { ReactElement } from 'react';
import styles from "../conversation.module.css";
import { Socket } from 'socket.io-client';
import { EMOJI, EVENT_NAMES } from '@/src/constants/chat';
import { useAppSelector } from '@/src/redux/hook';

type Reaction = {
    conversationSocket?: Socket<any, any>,
    messageId: string
}

export default function ReactionBox( { conversationSocket, messageId }: Reaction): ReactElement { 
    const { user } = useAppSelector(state => state.user);

    const reactMessage = (iconId: string) => {
        console.log("test emit");
        if(!conversationSocket) return;
        conversationSocket.emit(EVENT_NAMES.CONVERSATION.REACTION, {
            userId: user.id,
            icon: iconId,
            messageId
        }, () => {
            console.log("test socket");
        });
    };

    return (
        <div className={`absolute z-10 -top-8 flex gap-2 w-fit rounded-full h-fit py-2 px-2 ${styles.reaction_box}`}>
            {EMOJI.map((emote: any) => {
                return (
                    <div onClick={() => reactMessage(emote.id)} key={emote.id} className='w-10 h-10 cursor-pointer'><p className={`text-3xl absolute ${styles.reaction}`}>{emote.src}</p></div>
                );
            })}
        </div>
    );
}