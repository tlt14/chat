"use client";
import React, { ReactElement, useState, use, cache } from 'react';
import styles from "../conversation.module.css";
import Image from "next/image";
import { HiUserCircle, HiUsers } from "react-icons/hi";
import { FiSearch, FiChevronDown, FiChevronLeft } from "react-icons/fi";
import { BiSolidBell } from "react-icons/bi";
import { AiFillEye, AiFillPushpin } from "react-icons/ai";
import GroupIcon from "@/public/people.svg";
import DownloadIcon from "@/public/Download.svg";
import Circle from "@/public//Ellipse 12.svg";
import SecurityIcon from "@/public/security.svg";
import GaleryIcon from "@/public/gallery.svg";
import AddIcon from "@/public/add-square.svg";
import ExportIcon from "@/public/export.svg";
import LockIcon from "@/public/lock.svg";
import BlockIcon from "@/public/slash.svg";
import { BsFileEarmarkText, BsLink45Deg } from 'react-icons/bs';
import { useAppSelector } from '@/src/redux/hook';
import axios from 'axios';
import { IMessage } from '@/src/types/Message';
import { MEDIA_TYPE } from '@/src/constants/chat';
import moment from 'moment';
import { checkFile, getFileName } from '@/src/utils/messages/handleUrl';
interface Props {
    isOpenInfo: boolean,
    setIsOpenInfo: (isOpenInfo: boolean) => void,
    idConversation: string
}
interface FeatureState {
    file: boolean;
    media: boolean;
    link: boolean;
}

const fetchMedia = cache((id: string) => axios.get(`/api/conversations/media?id=${id}`));
export default function InfoChatMbl( { isOpenInfo, setIsOpenInfo, idConversation }: Props): ReactElement {
    const [option, setOption] = useState<number>(0);
    const { conversation } = useAppSelector(state => state.user);
    const { data } = use(fetchMedia(idConversation));
    console.log(data);
    const [isFile, setIsFile] = useState<boolean>(false);
    const [feature, setFeature] = useState<FeatureState>({
        file: true,
        media: false,
        link: false
    });
    const handleShowMenu = (id: number)=>{
        if(option === 0) setOption(id);
        else setOption(0);
    };
    console.log(isOpenInfo);
    return (
        <div className={`h-[92vh] relative bg-thDark-background flex-shrink-0 overflow-y-auto transform transition duration-500 ${isOpenInfo ? '-translate-x-full' : 'translate-x-0'} w-screen`}>
            <div className={`${isFile ? 'hidden' : 'block'}`}>
                <div className='absolute left-4 cursor-pointer' onClick={()=>setIsOpenInfo(false)}>
                    <FiChevronLeft size={40} />
                </div>
                <div className='w-full flex flex-col items-center gap-y-2'>
                    <Image 
                        src={conversation.otherAvatar}
                        sizes="(max-width: 96px) 96px, 96px"
                        alt={"avt"}
                        loading="eager"
                        priority
                        width={96}
                        height={96}
                        className="rounded-full object-cover h-24"
                    
                    />
                    <div className='flex flex-col items-center'>
                        <p className='text-lg font-semibold'>{conversation.otherUsername}</p>
                        <span className='opacity-60'>Full name</span>
                    </div>
                </div>
                <div className='flex gap-x-4 justify-center mt-4'>
                    <button className={`text-thDark-background flex px-2 py-2 rounded-md ${styles.button_profile}`}>
                        <HiUserCircle size={24} /> <span className='font-medium'>Trang cá nhân</span>
                    </button>
                    <button className={`text-thDark-background flex px-2 py-2 rounded-md bg-gray-300`}>
                        <FiSearch size={24} /> <span className='font-medium'>Tìm kiếm</span>
                    </button>
                </div>
                <div className='flex justify-center gap-x-1 flex-nowrap mt-4 px-6'>
                    <div className='flex flex-col items-center w-[76px]'>
                        <div className='bg-thNewtral1 w-fit rounded-full px-2 py-2'>
                            <BiSolidBell size={32} />
                        </div>
                        <span className='opacity-60 text-center'>Tắt thông báo</span>
                    </div>
                    <div className='flex flex-col items-center w-[76px]'>
                        <div className='bg-thNewtral1 w-fit rounded-full px-2 py-2'>
                            < AiFillEye size={32} />
                        </div>
                        <span className='opacity-60 text-center'>Ẩn đã xem</span>
                    </div>
                    <div className='flex flex-col items-center w-[76px]'>
                        <div className='bg-thNewtral1 w-fit rounded-full px-2 py-2'>
                            <AiFillPushpin size={32} />
                        </div>
                        <span className='opacity-60 text-center'>Ghim tin nhắn</span>
                    </div>
                    <div className='flex flex-col items-center w-[76px]'>
                        <div className='bg-thNewtral1 w-fit rounded-full px-2 py-2'>
                            <HiUsers size={32} />
                        </div>
                        <span className='opacity-60 text-center'>Thêm vào nhóm</span>
                    </div>
                </div>
                <div className='mt-8 px-6'>
                    <div className='flex justify-between py-3' onClick={()=>handleShowMenu(1)}>
                        <p className='font-bold'>Nhóm</p>
                        {
                            option === 1 ? (
                                <FiChevronDown size={24} />
                            ) :
                                (
                                    <Image 
                                        src={GroupIcon}
                                        sizes="(max-width: 24px) 24px, 24px"
                                        alt={"avt"}
                                        loading="eager"
                                        priority
                                        width={24}
                                        height={24}
                                        className="rounded-full object-cover h-6"
                                    />
                                )
                        }
                    </div>
                    <div className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${option === 1 && '!h-fit'}`}>
                        <div className='flex justify-between py-3'>
                            <p className='font-bold'>Nhóm chung</p>
                            <Image 
                                src={GroupIcon}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full object-cover h-6"
                            />
                        </div>
                        <div className='flex justify-between py-3'>
                            <p className='font-bold'>Tạo nhóm mới</p>
                            <Image 
                                src={AddIcon}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full object-cover h-6"
                            />
                        </div>
                    </div>
                    <div className='flex justify-between py-3' onClick={()=>setIsFile(true)}>
                        <p className='font-bold'>Files, phương tiện, liên kết</p>
                        <Image 
                            src={GaleryIcon}
                            sizes="(max-width: 24px) 24px, 24px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={24}
                            height={24}
                            className="rounded-full object-cover h-6"
                        />
                    </div>
                    <div className='flex justify-between py-3' onClick={()=>handleShowMenu(2)}>
                        <p className='font-bold'>Đổi ảnh nền</p>
                        {
                            option === 2 ? (
                                <FiChevronDown size={24} />
                            ) :
                                (<Image 
                                    src={Circle}
                                    sizes="(max-width: 24px) 24px, 24px"
                                    alt={"avt"}
                                    loading="eager"
                                    priority
                                    width={24}
                                    height={24}
                                    className="rounded-full object-cover h-6"
                                />)
                        }
                    </div>
                    <div className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${option === 2 && '!h-fit'}`}>
                        <div className='flex justify-between py-3'>
                            <p className='font-bold'>Ảnh từ thiết bị</p>
                            <Image 
                                src={ExportIcon}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full object-cover h-6"
                            />
                        </div>
                        <div className='flex justify-between py-3'>
                            <p className='font-bold'>Ảnh trên hệ thống</p>
                            <Image 
                                src={AddIcon}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full object-cover h-6"
                            />
                        </div>
                    </div>
                    <div className='flex justify-between py-3'>
                        <p className='font-bold'>Lưu cuộc trò chuyện</p>
                        <Image 
                            src={DownloadIcon}
                            sizes="(max-width: 24px) 24px, 24px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={24}
                            height={24}
                            className="rounded-full object-cover h-6"
                        />
                    </div>
                    <div className='flex justify-between py-3' onClick={()=>handleShowMenu(3)}>
                        <p className='font-bold'>Bảo mật và quyền riêng tư</p>
                        {
                            option === 3 ? (
                                <FiChevronDown size={24} />
                            ) :
                                (<Image 
                                    src={SecurityIcon}
                                    sizes="(max-width: 24px) 24px, 24px"
                                    alt={"avt"}
                                    loading="eager"
                                    priority
                                    width={24}
                                    height={24}
                                    className="rounded-full object-cover h-6"
                                />)
                        }
                    </div>
                    <div className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${option === 3 && '!h-fit'}`}>
                        <div className='flex justify-between py-3'>
                            <p className='font-bold'>Mã hóa</p>
                            <Image 
                                src={LockIcon}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full object-cover h-6"
                            />
                        </div>
                        <div className='flex justify-between py-3'>
                            <p className='font-bold'>chặn</p>
                            <Image 
                                src={BlockIcon}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full object-cover h-6"
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className={`${isFile ? 'block' : 'hidden'}`}>
                <div className='flex gap-x-4 items-center'>
                    <div className='cursor-pointer' onClick={()=>setIsFile(false)}>
                        <FiChevronLeft size={40} />
                    </div>
                    <p className='text-lg font-semibold'>Files, phương tiện, liên kết</p>
                </div>
                <div className='flex justify-around py-3 bg-thNewtral1 rounded mx-4'>
                    <p className={`${feature.media && 'text-thPrimary font-medium'}`} onClick={() => setFeature({ file:false, media: true, link:false })}>Phương tiện</p>
                    <p className={`${feature.file && 'text-thPrimary font-medium'}`} onClick={() => setFeature({ file:true, media: false, link:false })}>File</p>
                    <p className={`${feature.link && 'text-thPrimary font-medium'}`} onClick={() => setFeature({ file:false, media: false, link:true })}>Liên kết</p>
                </div>
                <div className={`${feature.media ? 'block' : 'hidden'} px-4 mt-4`}>
                    <div className='grid grid-cols-3 gap-4 py-4 w-full'>
                        {data.filter((message: IMessage) => message.type === MEDIA_TYPE.IMAGE && message.content.includes("storage.stechvn.org")).map((filteredMsg: IMessage, index: number) => {
                            return (
                                <div key={index}>
                                    {/* <p className='opacity-60 text-lg'>{moment(filteredMsg.createdAt).format("DD-MM-YYYY")}</p> */}
                                    <div>
                                        <Image sizes="(max-width: 96px) 96px, 96px"
                                            alt={"avt"}
                                            loading="eager"
                                            priority
                                            width={96}
                                            height={96} src={filteredMsg.content}  />
                                        {/* <a target="_blank" rel="noopener noreferrer" href={filteredMsg.content}  className='max-w-[80%] text-ellipsis overflow-x-hidden'>{getFileName(filteredMsg.content)}</a> */}
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
                <div className={`${feature.file ? 'block' : 'hidden'} px-4 mt-4`}>
                    <div className='flex flex-col gap-y-2'>
                        {data.filter((message: IMessage) => message.type === MEDIA_TYPE.FILE && message.content.includes("storage.stechvn.org") && checkFile(message.content)).map((filteredMsg: IMessage, index: number) => {
                            return (
                                <div key={index}>
                                    <p className='opacity-60 text-lg'>{moment(filteredMsg.createdAt).format("DD-MM-YYYY")}</p>
                                    <div key={index} className="py-4 bg-thNewtral1 flex items-center gap-x-4 h-fit rounded-md px-4">
                                        <BsFileEarmarkText size={40}/>
                                        <a target="_blank" rel="noopener noreferrer" href={filteredMsg.content}  className='max-w-[80%] text-ellipsis overflow-x-hidden'>{getFileName(filteredMsg.content)}</a>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
                <div className={`${feature.link ? 'block' : 'hidden'} px-4 mt-4`}>
                    <div className='flex flex-col gap-y-2'>
                        {data.filter((message: IMessage) => message.type === MEDIA_TYPE.LINK).map((filteredMsg: IMessage, index: number) => {
                            return (
                                <div key={index}>
                                    <p className='opacity-60 text-lg'>{moment(filteredMsg.createdAt).format("DD-MM-YYYY")}</p>
                                    <div key={index} className="py-4 bg-thNewtral1 flex items-center gap-x-4 h-fit rounded-md px-4">
                                        <BsLink45Deg size={40}/>
                                        <a target="_blank" rel="noopener noreferrer" href={filteredMsg.content}  className='max-w-[80%] text-ellipsis overflow-x-hidden'>{filteredMsg.content}</a>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
}