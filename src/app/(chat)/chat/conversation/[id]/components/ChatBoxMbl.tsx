/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import React, { ReactElement, useEffect, useState, useRef } from 'react';
import phone from "@/public/call.svg";
import videoCall from "@/public/video-call.svg";
import info from "@/public/info-circle.svg";
import Slute from "@/public/slute.png";
import Send from "@/public/send-2.svg";
import Image from "next/image";
import styles from "../conversation.module.css";
import { AiOutlineSmile } from "react-icons/ai";
import { BsImage, BsFileEarmarkText } from "react-icons/bs";
import dynamic from 'next/dynamic';
import { Theme } from 'emoji-picker-react';
import PartnerText from './PartnerText';
import OwnerText from './OwnerText';
import Forward from './Forward';
import InfoChatMbl from './InfoChatMbl';
import CallMbl from './CallMbl';
import { IMessage, IMessageData } from '@/src/types/Message';
import { useAppDispatch, useAppSelector } from '@/src/redux/hook';
import { EVENT_NAMES, MESSAGE_TYPE } from '@/src/constants/chat';
import axios from 'axios';
import useCreateSocket from '@/src/hook/useCreateSocket';
import { Socket } from 'socket.io-client';
import moment from 'moment';
import { setLastMessage } from '@/src/redux/slices/conversationSlice';
import { sendMessage } from '@/src/utils/messages/sendMessage';

const Picker = dynamic(
    () => {
        return import('emoji-picker-react');
    },
    { ssr: false }
);
interface ChatBoxProps {
    listMessage: IMessage[],
    idConversation: string
}
export default function ChatBoxMbl({ listMessage, idConversation }: ChatBoxProps): ReactElement { 
    const { user, conversation } = useAppSelector(state => state.user);
    const dispatch = useAppDispatch();
    const [page, setPage] = useState<number>(2);  
    const [messages, setMessages] = useState<IMessage[]>(listMessage);
    const [isBottom, setIsBottom] = useState<boolean>(false);
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const [isOpenEmoji, setIsOpenEmoji] = useState<boolean>(false);
    const [isOpenInfo, setIsOpenInfo] = useState<boolean>(false);
    const [isCalling, setIsCalling] = useState<boolean>(false);
    const [forwardContent, setForwardContent] = useState<IMessage | any>({});
    const divRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    const socket: Socket<any, any> = useCreateSocket(idConversation);
    const [reactionID, setReactionID] = useState<string>('');
    useEffect(() => {
        if(divRef.current && !isBottom) {
            divRef.current.scrollTop = divRef.current.scrollHeight; 
        }
    }, [messages]);

    useEffect(() => {
        (async () => {
            if(!isBottom) return; 
            console.log(page);
            const response = await axios.get(`/api/conversations/messages?id=${idConversation}&page=${page}`);
            console.log(response.data);
            if(response.data) {                
                setMessages([...response.data, ...messages]);
                setPage(page + 1);
            }
        })();
    }, [isBottom]); 
   
    useEffect(() => {
        const handleAddMessages = () => {
            if(!divRef.current) return; 
            const divEl = divRef.current;
            const isBottomScroll = divEl.scrollTop === 0;
            setIsBottom(isBottomScroll);
        };  
        divRef.current?.addEventListener("scroll", handleAddMessages);
        return () => {
            divRef.current?.removeEventListener("scroll", handleAddMessages);
        };
    }, []);
    
    //add emoji to input
    const onEmojiClick = (emojiObject: any) => {
        if (inputRef.current !== null) {
            const value = inputRef.current.value;
            const selectionStart = inputRef.current.selectionStart;
            // Thêm emoji vào vị trí con trỏ bất kỳ
            if (selectionStart !== null) {
                const updatedValue = value.substring(0, selectionStart) + emojiObject.emoji + value.substring(selectionStart);
                // Cập nhật giá trị và vị trí con trỏ mới
                if (inputRef.current !== null) {
                    inputRef.current.value = updatedValue;
                    inputRef.current.setSelectionRange(selectionStart + emojiObject.emoji.length, selectionStart + emojiObject.emoji.length);
                    inputRef.current.focus();
                }
            }
        }
        console.log("emoji", emojiObject.emoji);
    };
    const handleSubmit = () => {
        const messageData: IMessageData = {
            content: inputRef.current?.value || "",
            replyTo: null,
            conversationId: idConversation,
            type: MESSAGE_TYPE.TEXT  
        };
        sendMessage(EVENT_NAMES.CONVERSATION.SEEN_MESSAGE, socket, () => {
            if(inputRef.current !== null)
                inputRef.current.value = "";
        }, messageData);
    };

    useEffect(() => {
        if(!socket) return;
        socket.on(EVENT_NAMES.CONVERSATION.SENDED, (msg: IMessage): any => {
            console.log(msg);
            setMessages((prevState) => [...prevState, msg]);
            dispatch(setLastMessage(msg));
        });
        
        return () => {
            socket.off(EVENT_NAMES.CONVERSATION.SENDED);
        };
    }, [socket]);
    

    useEffect(() => {
      
        console.log(!user.socket);
        if(!user.socket) return;
        user.socket.on(EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE, (message: any) => {
            console.log(message);
        });
        return () => {
            user.socket.off(EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE);
        };
    }, [user.socket]);

    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            handleSubmit();
        }
    };
    const handleSlute = () =>{
        const messageData: IMessageData = {
            content: `${process.env.NEXT_PUBLIC_DAK_UPLOAD_API}/2a0c23fa6-4590-11ee-9971-0242ac130003.slute.png`,
            replyTo: null,
            conversationId: idConversation,
            type: MESSAGE_TYPE.STICKER  
        };
        sendMessage(EVENT_NAMES.CONVERSATION.SEEN_MESSAGE, socket, () =>  {}, messageData);
    };
    const handleCall = ()=>{
        setIsCalling(true);
    };
    return (
        <>
            <div className={`w-screen overflow-x-hidden flex`}>
                <div className={`h-[92vh] flex flex-col relative flex-shrink-0 w-screen`}> 
                    <div className='chat_header py-2 px-4 bg-thNewtral1 flex justify-between items-center'>
                        <div className='flex gap-x-2 items-center'>
                            <div className='relative'>
                                <Image 
                                    src={conversation?.otherAvatar || ""}
                                    sizes="(max-width: 44px) 44px, 44px"
                                    alt={"avt"}
                                    loading="eager"
                                    priority
                                    width={44}
                                    height={44}
                                    className="rounded-full object-cover h-11"
                                />
                                <div className='bg-thPrimary w-3 h-3 rounded-full absolute bottom-0 right-0 border-2 border-thNewtral1'></div>
                            </div>
                            <div className='flex flex-col text-thWhite'>
                                <p className='text-base font-semibold'>User name</p>
                                <span className='text-xs font-normal opacity-70'>online now </span>
                            </div>
                        </div>
                        <div className='flex gap-x-3 items-center'>
                            <Image 
                                src={phone}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleCall}
                            />
                            <Image 
                                src={videoCall}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleCall}
                            />
                            <Image 
                                src={info}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={()=>setIsOpenInfo(true)}
                            />
                        </div>
                    </div>
                    {/* nếu đang mở socket gọi thì nhớ sửa lại điều kiện ở đoạn dưới đây */}
                    {isCalling && (<p className='w-full bg-thPrimary py-1 text-thDark-background text-center' onClick={() => setIsCalling(true)}>Cuộc gọi đang diễn ra quay lại cuộc gọi...</p>)}
                    <div className={`h-full flex flex-col overflow-y-auto snap-end py-3 ${styles.inboxList}`} ref={divRef} onClick={() => setIsOpenEmoji(false)}>
                        <div className='mt-auto'>
                            <div className='socket_conversation flex flex-col gap-y-2 px-4'>
                                {
                                    messages.map((message: IMessage, index: number) => {
                                        const curMsgDate = moment(message.createdAt).format(
                                            "DD-MM-YYYY"
                                        );
                                        const nextMsgDate = messages[index + 1] && moment(messages[index + 1].createdAt).format("DD-MM-YYYY");
                                        return (
                                            <div key={index}>
                                                {index === 0 && (
                                                    <div className='my-6'>
                                                        <p className='text-sm w-fit mx-auto bg-thNewtral2 px-1.5 py-1 rounded opacity-60'>{nextMsgDate}</p>
                                                    </div>
                                                )}
                                                {message.createdBy._id !== user.id ? (
                                                    <div key={message.createdBy._id}>
                                                        <PartnerText socket={socket} reactionID={reactionID} setReactionID={setReactionID} message={message} setForwardContent={setForwardContent}/>
                                                    </div>
                                                )
                                                    :
                                                    (
                                                        <div key={message.createdBy._id}>
                                                            <OwnerText socket={socket} reactionID={reactionID} setReactionID={setReactionID} isLastMsg={messages[messages.length - 1]._id === message._id} message={message} setForwardContent={setForwardContent}/>
                                                        </div>
                                                    )
                                                }
                                                {index >= 1 && curMsgDate !== nextMsgDate && nextMsgDate && (
                                                    <div className='my-6'>
                                                        <p className='text-sm w-fit mx-auto bg-thNewtral2 px-1.5 py-1 rounded opacity-60'>{nextMsgDate}</p>
                                                    </div>
                                                )}   
                                            </div>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className='absolute bottom-16 right-6'>
                        {
                            isOpenEmoji && (<Picker onEmojiClick={onEmojiClick} height={337} width={340} searchDisabled={true} theme={Theme.DARK}/>)
                        }
                    </div>
                    {
                        Object.keys(forwardContent).length > 0 && (<>
                            <div className={`overlay absolute h-full w-full z-10 ${styles.glass} cursor-pointer`} onClick={() => setForwardContent({})}></div>
                            <div className='absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-50'>
                                <Forward message={forwardContent} socket={socket}/>
                            </div>
                        </>)
                    }
            
                    <div className='chat_area mt-auto py-4 px-4 flex gap-x-4 items-center'>
                        <div className=' flex items-center justify-between w-full'>
                            <div className='bg-thNewtral1 w-full py-3 px-2 flex gap-x-4 justify-between items-center rounded-2xl'>
                                <input type="text" ref={inputRef} className='px-4 outline-none border-none w-full bg-transparent' placeholder='nhập gì đó...' onFocus={()=>setIsFocused(true)}
                                    onBlur={()=>setIsFocused(false)} onKeyDown={handleKeyDown}/>
                                <ul className={`flex items-center gap-x-3 opacity-60 ${styles.input_feature}`}>
                                    <li onClick={()=>setIsOpenEmoji(!isOpenEmoji)}><AiOutlineSmile size={20}/></li>
                                    <li><BsImage size={20}/></li>
                                    <li><BsFileEarmarkText size={20}/></li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            {
                                isFocused ? (
                                    <div onClick={handleSubmit}>
                                        <Image 
                                            src={Send}
                                            sizes="(max-width: 40px) 40px, 40px"
                                            alt={"avt"}
                                            loading="eager"
                                            priority
                                            width={40}
                                            height={40}
                                            className="rounded-full h-10 cursor-pointer"
                                        />
                                    </div>
                                ) :
                                    (
                                        <div onClick={handleSlute}>
                                            <Image 
                                                src={Slute}
                                                sizes="(max-width: 40px) 40px, 40px"
                                                alt={"avt"}
                                                loading="eager"
                                                priority
                                                width={40}
                                                height={40}
                                                className="rounded-full h-10 cursor-pointer"
                                            />
                                        </div>
                                    )
                            }  
                        </div>
                    </div>
                </div>
                <InfoChatMbl idConversation={idConversation} isOpenInfo={isOpenInfo} setIsOpenInfo={setIsOpenInfo}/> 
            </div>
            {/* call component */}
            {isCalling && (<CallMbl setIsCalling={setIsCalling}/> )}
        </>
    );
};