"use client";
import React, { ReactElement, useState } from 'react';
import styles from "../conversation.module.css";
import Image from "next/image";
import avt2 from "@/public/pattern3.jpg";
import { useAppSelector } from "@/src/redux/hook";
import { BiSolidMicrophoneOff } from "react-icons/bi";
import { BsFillCameraVideoFill } from "react-icons/bs";
import { HiSpeakerWave, HiSpeakerXMark } from "react-icons/hi2";
import phone from "@/public/call2.svg";

interface Props {
    isCalling: boolean,
    setIsCalling: (isCalling: boolean) => void,
    avatar: string
}
interface FeatureState {
    mute: boolean;
    camera: boolean;
    speaker: boolean;
  }


export default function Call({ isCalling, setIsCalling, avatar  }: Props ): ReactElement {
    const { user }  = useAppSelector((state) => state.user);
    const [feature, setFeature] = useState<FeatureState>({
        mute: false,
        camera: false,
        speaker: false
    });
    console.log("avatar", isCalling);
    return (
        <div className={` w-full relative`}>
            <div className={`absolute h-full w-full ${styles.call_backgound_desktop}`}>
                <Image 
                    src={avatar || ""}
                    sizes="120vw"
                    fill
                    alt={"bg"}
                    loading="eager"
                    priority
                    className={`object-cover`}   
                />
            </div>
            <div className='absolute w-full h-full z-10'>
                <div className='flex justify-between pr-7 pl-3 pt-3'>
                    <div className={`flex items-center gap-x-1 ${styles.time} px-2 py-1 rounded h-fit `}>
                        <div className='bg-thRed w-2 h-2 rounded-full'></div>
                        <p className={`text-xs font-normal`}>01:30:08</p>
                    </div>
                    <div className=''>
                        <Image 
                            src={user.avatar !== "" ? user.avatar : avt2}
                            sizes="300px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={144}
                            height={208}
                            className="rounded-lg object-cover h-52"
                        />
                    </div>
                </div>
                <div className='feature flex justify-center w-full absolute z-50 bottom-4 gap-x-4'>
                    <div className={`rounded-full ${styles.call_btn} ${feature.mute === true && styles.call_btn_active}`} onClick={()=>setFeature(prevState => ({ ...prevState, mute: !prevState.mute }))}>< BiSolidMicrophoneOff  size={24}/></div>
                    <div className={`rounded-full ${styles.call_btn} ${feature.camera === true && styles.call_btn_active}`} onClick={()=>setFeature(prevState => ({ ...prevState, camera: !prevState.camera }))}>< BsFillCameraVideoFill size={24}/></div>
                    <div className={`rounded-full ${styles.call_btn} ${feature.speaker === true && styles.call_btn_active}`} onClick={()=>setFeature(prevState => ({ ...prevState, speaker: !prevState.speaker }))}>
                        {
                            feature.speaker === true ? (< HiSpeakerXMark size={24}/>) : (< HiSpeakerWave size={24}/>)
                        } 
                    </div>
                    <div className={`rounded-full !bg-thRed ${styles.call_btn}`} onClick={()=>setIsCalling(false)}>
                        <Image 
                            src={phone}
                            sizes="32px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={24}
                            height={24}
                            className="rounded-full h-6 cursor-pointer text-thWhite"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}