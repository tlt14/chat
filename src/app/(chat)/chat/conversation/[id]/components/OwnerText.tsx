import React, { ReactElement, useState } from 'react';
import styles from "../conversation.module.css";
import { AiFillMessage, AiFillSmile } from "react-icons/ai";
import { RiShareForwardFill } from "react-icons/ri";
import Image from 'next/image';
import { EMOJI, MESSAGE_TYPE } from '@/src/constants/chat';
import moment from 'moment';
import { IMessage } from '@/src/types/Message';
import ReactionBox from './ReactionBox';
import { Socket } from 'socket.io-client';

interface Text {
    message: IMessage
    isLastMsg: boolean,
    setForwardContent: (forwardContent: IMessage) => void,
    reactionID: string, 
    setReactionID: (reactionID: string) => void,
    socket?: Socket<any, any>
}
export default function OwnerText({ isLastMsg, message, setForwardContent, reactionID, setReactionID, socket  }: Text): ReactElement {
    const [isReact, setIsReact] = useState<boolean>(false);

    const handleReaction = () =>{
        if(isReact === true && reactionID === message?._id) {
            setIsReact(false); 
            setReactionID('');
            return 0;
        }
        if (isReact === true && reactionID !== message?._id) {
            setReactionID(message?._id);
            return 0;
        }
        
        setIsReact(true); 
        setReactionID(message?._id);
        return 0;
    };

    return (
        <>
            <div className={`flex w-full gap-6 justify-end ${styles.text_box} relative`}>
                {
                    isReact && reactionID === message?._id && (
                        <ReactionBox messageId={message._id} conversationSocket={socket}  />
                    )
                }
                <div className={`${styles.icon_area} flex items-center gap-x-2 opacity-25`}>
                    <RiShareForwardFill size={20} onClick={() => setForwardContent(message)}/>
                    <AiFillMessage size={20} />
                    <AiFillSmile size={20} onClick={handleReaction}/>
                </div>
                <div className={`bg-thPrimary text-thDark-background w-fit p-3 ${styles.owner_box} max-w-[224px] md:max-w-[380px] lg:max-w-[564px]`}>
                    {message.type === MESSAGE_TYPE.STICKER  || message.type === MESSAGE_TYPE.IMAGE ? (
                        <Image alt={message.content} src={message.content} width={40} height={40} />
                    ) : message.type === MESSAGE_TYPE.TEXT ? (
                        <span className='text-[.9375rem]'>{message.content}</span>
                    ) : null}
                    <div className='flex justify-between items-center gap-x-2 mt-2'>
                    
                        {message.reactions.length > 0 && (
                            <div className="flex justify-center items-center gap-2 bg-green-600 rounded-lg px-1.5 py-1">
                                {message.reactions.map((react: any, index: number) => {
                                    if(index <= 1) {
                                        return (
                                            <span key={index}>
                                                {EMOJI.find(e => e.id === react.icon)?.src || ""}
                                            </span>
                                        );
                                    
                                    }
                                })}
                                {message.reactions.length > 1 && ( <span className='text-[12px] opacity-60'>{message.reactions.length}</span>)}
                            </div>
                        )}
                        <span className='text-[.775rem] opacity-80'>{moment(message.createdAt).format("HH:mm")}</span>
                    </div>
                </div>
            </div>
            <div className='text-right w-full opacity-40 text-xs py-1'>{isLastMsg && "Đã xem"}</div>
        </>
    );
}