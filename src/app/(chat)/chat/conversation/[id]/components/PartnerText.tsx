import React, { ReactElement, useState } from 'react';
import styles from "../conversation.module.css";
import { AiFillMessage, AiFillSmile } from "react-icons/ai";
import { RiShareForwardFill } from "react-icons/ri";
import { EMOJI, MESSAGE_TYPE } from '@/src/constants/chat';
import Image from 'next/image';
import moment from 'moment';
import { IMessage } from '@/src/types/Message';
import ReactionBox from './ReactionBox';
import { Socket } from 'socket.io-client';
interface Text {
    message: IMessage,
    setForwardContent: (forwardContent: IMessage) => void,
    reactionID: string, 
    setReactionID: (reactionID: string) => void,
    socket?: Socket<any, any>

}

export default function PartnerText({ message, setForwardContent, reactionID, setReactionID, socket }: Text): ReactElement { 
    const [isReact, setIsReact] = useState<boolean>(false);
    const handleReaction = () =>{
        if(isReact === true && reactionID === message?._id) {
            setIsReact(false); 
            setReactionID('');
            return 0;
        }
        if (isReact === true && reactionID !== message?._id) {
            setReactionID(message?._id);
            return 0;
        }
        
        setIsReact(true); 
        setReactionID(message?._id);
        return 0;
        
    };
    return (
        <div className={`flex w-full gap-6 ${styles.text_box} relative`}>
            {
                isReact && reactionID === message?._id && (
                    <ReactionBox messageId={message._id} conversationSocket={socket}  />
                )
            }
            <div className='flex items-end gap-x-1'>
                <div className={`bg-thNewtral md:bg-thDark-background lg:bg-thDark-background text-thWhite w-fit p-3 ${styles.partner_box} max-w-[224px] md:max-w-[380px] lg:max-w-[564px]`}>
                    {message.type === MESSAGE_TYPE.TEXT ? (
                        <span className='text-[.9375rem] opacity-80'>{message?.content}</span>
                    ) : message.type === MESSAGE_TYPE.IMAGE || message.type === MESSAGE_TYPE.STICKER ? (
                        <>
                            <Image src={message?.content} alt={message._id} width={400} height={400} />
                        </>
                    ) :  <span className='text-[.9375rem] opacity-80'>{message?.content}</span>}
                    <div className='flex justify-between items-center gap-x-2 mt-2'>
                        <span className='text-[.775rem] opacity-50'>{moment(message?.createdAt).format("HH:mm")}</span>
                        {message?.reactions?.length > 0 && (
                            <div className="flex justify-center items-center gap-2 bg-thNewtral1 rounded-lg px-1.5 py-1">
                                {message?.reactions?.map((react: any, index: number) => {
                                    if(index <= 1) {
                                        return (
                                            <span key={index}>
                                                {EMOJI.find(e => e.id === react.icon)?.src || ""}
                                            </span>
                                        );
                                    }
                                })}
                                <span className='text-[12px] opacity-60'>{message?.reactions?.length}</span>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <div className={`${styles.icon_area} flex items-center gap-x-2 opacity-25 `}>
                <AiFillSmile size={20} onClick={handleReaction}/>
                <AiFillMessage size={20} />
                <RiShareForwardFill size={20} onClick={() => setForwardContent(message)}/>
            </div>
        </div>
    );
}