"use client";
import React, { ReactElement, useState } from 'react';
import styles from "../conversation.module.css";
import Image from "next/image";
import avt from "@/public/pattern.png";
import avt2 from "@/public/pattern3.jpg";
import { FiChevronLeft } from "react-icons/fi";
import { BiSolidMicrophoneOff } from "react-icons/bi";
import { BsFillCameraVideoFill } from "react-icons/bs";
import { HiSpeakerWave } from "react-icons/hi2";
import phone from "@/public/call2.svg";

interface Props {
    setIsCalling: (isOpenInfo: boolean) => void
}
interface FeatureState {
    mute: boolean;
    camera: boolean;
    speaker: boolean;
  }
export default function CallMbl({ setIsCalling }: Props ): ReactElement {
    const [feature, setFeature] = useState<FeatureState>({
        mute: false,
        camera: false,
        speaker: false
    });
    return (
        <div className='absolute -top-3 left-0 bg-thDark-background h-screen w-screen overflow-x-hidden'>
            <div className={`${styles.call_backgound} absolute`}>
                <Image 
                    src={avt}
                    sizes="120vw"
                    alt={"bg"}
                    loading="eager"
                    priority
                    className={`object-cover h-screen `}   
                />
            </div>
            <div className='absolute z-30 w-full'>
                <div className='flex items-start justify-between pr-3 mt-6'>
                    <div className='flex gap-x-2 items-center'>
                        <div className='' onClick={()=>setIsCalling(false)}>
                            <FiChevronLeft size={40} />
                        </div>
                        <div className='flex flex-col'>
                            <p className='text-lg font-semibold'>User name</p>
                            <span className=''>00:47:36</span>
                        </div>
                    </div>
                    <div className=''>
                        <Image 
                            src={avt2}
                            sizes="144px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={144}
                            height={208}
                            className="rounded-lg object-cover h-52"
                        />
                    </div>
                </div>
            </div>
            <div className='feature flex justify-center w-full absolute z-50 bottom-4 gap-x-4'>
                <div className={`rounded-full ${styles.call_btn} ${feature.mute === true && styles.call_btn_active}`} onClick={()=>setFeature(prevState => ({ ...prevState, mute: !prevState.mute }))}>< BiSolidMicrophoneOff  size={32}/></div>
                <div className={`rounded-full ${styles.call_btn} ${feature.camera === true && styles.call_btn_active}`} onClick={()=>setFeature(prevState => ({ ...prevState, camera: !prevState.camera }))}>< BsFillCameraVideoFill size={32}/></div>
                <div className={`rounded-full ${styles.call_btn} ${feature.speaker === true && styles.call_btn_active}`} onClick={()=>setFeature(prevState => ({ ...prevState, speaker: !prevState.speaker }))}>< HiSpeakerWave size={32}/></div>
                <div className={`rounded-full !bg-thRed ${styles.call_btn}`}>
                    <Image 
                        src={phone}
                        sizes="32px"
                        alt={"avt"}
                        loading="eager"
                        priority
                        width={32}
                        height={32}
                        className="rounded-full h-8 cursor-pointer text-thWhite"
                    />
                </div>
            </div>
        </div>
    );
}