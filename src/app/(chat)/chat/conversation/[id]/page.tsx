"use client";
import React, { ReactElement, use, cache } from 'react';
import ChatBox from './components/ChatBox';
import useWindowDimensions from "../../../../../hook/useWindowDimension";
import ChatBoxMbl from './components/ChatBoxMbl';
// import { IMessage } from '@/src/types/Message';
import axios from 'axios';


const fetchMessages = cache((id: string) => axios.get(`/api/conversations/messages?id=${id}&page=1`));

export default function Page({ params }: { params: { id: string } }): ReactElement { 
    const { width } = useWindowDimensions();
    const { data } = use(fetchMessages(params.id));
    console.log(data);
    return (
        <>
            {
                width > 480 ? <ChatBox idConversation={params.id} listMessage={data} /> : <ChatBoxMbl idConversation={params.id} listMessage={data} />
            }
        </>       
        
    );
}
