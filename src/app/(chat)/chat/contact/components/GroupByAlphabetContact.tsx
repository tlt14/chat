/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { ReactElement } from 'react';
import ChatCallIcon from '@/public/call.svg';
import VideoCallIcon from '@/public/video-call.svg';
import Image from 'next/image';
import { IUserProfile } from '@/src/types/User';
import { Avatar } from '../../components/Avatar';
import { useAppDispatch, useAppSelector } from '@/src/redux/hook';
import { useRouter } from 'next/navigation';
import { setOtherUser } from '@/src/redux/slices/conversationSlice';

type ListUser = {
    user: IUserProfile
}

interface GroupByAlphabetContactProps {
    title: string;
    contactData: ListUser[];
}

const GroupByAlphabetContact = ( { title, contactData }: GroupByAlphabetContactProps): ReactElement => {

    const actions = [
        {
            name: 'chat call',
            icon: ChatCallIcon,
            onClick: () => {}
        },
        {
            name: 'video call',
            icon: VideoCallIcon,
            onClick: () => {}
        }
    ];

    const { push } = useRouter();
    const { conversation }  = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();

    const directConversation = (contact: any) => {
        const selectedConversation: any = conversation.listConversation.find((conver: any) =>  conver?.idOther === contact.user.id);
        console.log(selectedConversation?.name);
        dispatch(setOtherUser({
            otherUsername: selectedConversation?.name,
            otherAvatar: selectedConversation?.avatar,
            lastMessage: conversation.lastMessage || {}
        }));
        push(`/chat/conversation/${selectedConversation?._id}`);
    };
    
    return (
        <div className='flex flex-col'>
            <span className='font-bold leading-6 mb-3 pl-8'>{ title }</span>
            {contactData.map((contact, index) => (
                // contact item wrapper
                <div onClick={() => directConversation(contact)} key={index} className='hover:bg-thNewtral1 cursor-pointer'>
                    {/* contact item */}
                    <div className='flex justify-between items-center py-3 px-8'>
                        {/* user infor */}
                        <div  className='flex items-center gap-3'>
                            <div className="relative w-14 h-14 overflow-hidden rounded-full">
                                <Avatar url={contact.user.avatar} />
                            </div>
                            <span className='font-bold leading-6'>
                                {contact.user.name}
                            </span>
                        </div>
                        {/* action */}
                        <div className='flex items-center gap-2'>
                            {
                                actions.map((action) => (
                                    <div key={`index-${action.name}`} className='hover:bg-thPrimaryHover rounded-md'>
                                        <Image src={action.icon} alt={action.name} width={28} className='m-2' />
                                    </div>
                                ))
                            }
                        </div>                 
                    </div>
                </div>
            ))}
        </div>
    );
};

export default GroupByAlphabetContact;