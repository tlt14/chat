/* eslint-disable @typescript-eslint/no-unused-vars */
"use client";
import React, { ReactElement, useState, useEffect, ChangeEvent, cache, use } from 'react';
import SortSelectBox from './SortSelectBox';
import GroupByAlphabetContact from './GroupByAlphabetContact';
import { BiSearch } from "react-icons/bi";
import { IoMdCloseCircle } from "react-icons/io";
import SearchNoResult from './SearchNoResult';
import { IUserProfile } from '@/src/types/User';
import axios from 'axios';

type SortOrder = 'A-Z' | 'Z-A';

type ListUser = {
    user: IUserProfile
}

interface GroupedContactsProps {
    [letter: string]: ListUser[];
}

const ListContact = (): ReactElement => {
    
    const [sortType, setSortType] = useState<SortOrder>('A-Z'); // danh bạ mặc định từ a đến z
    const [sortedAlphabetList, setSortedAlphabetList] = useState<GroupedContactsProps>({});
    const [searchTerm, setSearchTerm] = useState<string>('');

    const [contactList, setContactList] = useState<ListUser[]>([]);
    
    useEffect(() => {
        // Fetch contact data
        axios.get(`/api/contacts`)
            .then(response => {
                setContactList(response.data.data);
            })
            .catch(error => {
                console.error('Error fetching contact data:', error);
            });
    }, []);
    
    useEffect(() => {
        const groupedContacts: GroupedContactsProps = {};
        if(contactList.length === 0) return;
        for (const contact of contactList) {
            const firstLetter = contact?.user?.name[0].toUpperCase();
            if (!groupedContacts[firstLetter]) {
                groupedContacts[firstLetter] = [];
            }
            groupedContacts[firstLetter].push(contact);
        }

        const sortedKeys = sortType === 'A-Z' ? Object.keys(groupedContacts || {}).sort() : Object.keys(groupedContacts || {}).sort().reverse();
        const sortedContacts: GroupedContactsProps = {};
        
        // sắp xếp theo letter
        for (const letter of sortedKeys) {
            sortedContacts[letter] = groupedContacts[letter];
            // sắp xếp theo tên bên trong
            for (const letter in sortedContacts) {
                if (groupedContacts.hasOwnProperty(letter)) {
                    sortedContacts[letter] = sortType === 'A-Z'
                        ? groupedContacts[letter].sort((a, b) => a.user.name.localeCompare(b.user.name))
                        : groupedContacts[letter].sort((a, b) => b.user.name.localeCompare(a.user.name));
                }
            }
        }     
        setSortedAlphabetList(sortedContacts);

    }, [sortType, contactList]);

    const handleSearch = (e: ChangeEvent<HTMLInputElement>): void => {
        setSearchTerm(e.target.value);
    };

    const filteredAlphabetList: GroupedContactsProps = Object.fromEntries(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        Object.entries(sortedAlphabetList).filter(([letter, contacts]) =>
            contacts.some(contact => contact.user.name.toLowerCase().includes(searchTerm.toLowerCase()))
        )
    );
    
    return (
        
        <div className='flex flex-col gap-8 relative h-full'>
            {/* filter container */}
            <div className='flex gap-4 flex-col md:flex-row mx-8'>
                {/* search bar */}
                <div className='flex items-center border border-thNewtral1 rounded-md relative w-80'>
                    <BiSearch className='ml-3 text-[#b5b5b5]'/>
                    <input 
                        type="text" 
                        placeholder="Tìm bạn"
                        value={searchTerm}
                        onChange={handleSearch}
                        className='bg-transparent outline-none p-2 w-full pr-12 pl-3'
                    />
                    {searchTerm !== "" &&
                        <IoMdCloseCircle 
                            className='absolute right-3 cursor-pointer text-[#b5b5b5] hover:text-white' 
                            size={24}
                            onClick={() => setSearchTerm("")} />
                    }
                </div>
                {/* select bar */}
                <SortSelectBox sortType={sortType} setSortType={setSortType} />
            </div>
            {
                Object.keys(filteredAlphabetList).length === 0 
                    ? <SearchNoResult />
                    : Object.entries(filteredAlphabetList).map(([letter, contacts]) => (
                        <GroupByAlphabetContact key={letter} title={letter} contactData={contacts.filter(contact => contact.user.name.toLowerCase().includes(searchTerm.toLowerCase()))} />
                    ))
            }
        </div>
    );
};

export default ListContact;