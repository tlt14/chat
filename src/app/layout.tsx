"use client";
import { ReactElement } from "react";
import "./globals.css";
import { Provider } from "react-redux";
import { store } from "../redux/store";
import { persistStore } from "redux-persist";

persistStore(store);
export default function RootLayout({
    children
}: {
    children: React.ReactNode;
}): ReactElement {
    
    return (
        <Provider store={store}>
            <html lang='en'>
                <body className='relative'>
                    {children}
                </body>
            </html>
        </Provider>
    );
}
