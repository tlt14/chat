import axios from "axios";
import { useEffect, useState } from "react";
import { Socket, io } from "socket.io-client";

const useCreateSocket = (idConversation: string): any => {
    const [socket, setSocket] = useState<Socket<any, any> | any>(null);
    useEffect(() => {
        (async () => {
            try {
                const response = await axios.get(`/api/auth/chat-token`);
                if(!response) return;
                console.log(process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET);
                const socketConversation = io(`${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}/chat-${idConversation}`, {
                    auth: { token: response.data },
                    autoConnect: true,
                    transports: ["websocket"],
                    withCredentials: true
                });
                setSocket(socketConversation);
            } catch(error: any) {
                console.log("Không thể gội socket");
                throw new Error(error);
            } 
        })();
    }, [idConversation]);
    return socket;
};

export default useCreateSocket;